import React, { useEffect, useState } from 'react'
import ChartOptions from '../ui/chartOptions/chartOptions'
import Chart from '../ui/chart/chart'

interface ChartDateInterface {
    yAxisName: string,
    colorPallete: string[],
    showTooltip: boolean,
    showLabel: boolean,
    labelPrefix: string,
    type: string,
    data: any
}
export default function ChartMain() {
    const [chartData, setChartData] = useState<ChartDateInterface>({
        yAxisName: 'Values',
        colorPallete: ['#000'],
        showTooltip: false,
        showLabel: false,
        labelPrefix: '',
        type: 'line',
        data: []
    })
    useEffect(() => {
        fetch('http://localhost:8080/getData')
            .then(response => response.json())
            .then(data => {
                console.log(data);
                setChartData(chartData => ({...chartData, 'data': data}))
    })
}, [])

const changeChartData = (valueTitle: string | number, value: any) => {
    setChartData({ ...chartData, [valueTitle]: value })
    console.log(chartData);

}
const chartOptopns = (type: string) => ({
    chart: {
        type,
        width: 500,
        height: 300,
    },
    title: {
        text: `${type} chart`,
    },
    tooltip: {
        enabled: chartData.showTooltip,
    },
    colors: chartData.colorPallete,
    yAxis: {
        title: {
            text: chartData.yAxisName,
        },
    },
    series: [
        {
            data: chartData.data,
            dataLabels: {
                enabled: chartData.showLabel,
            },

        },
    ],
});
return (
    <div className="flex flex-row-reverse	ps-11">
        <div className="ms-11 p-10 bg-black text-white flex  items-center flex-col">
            <ChartOptions changeOptions={changeChartData} data={chartData}></ChartOptions>
        </div>
        <div className="">
            <Chart options={chartOptopns} data={chartData}></Chart>
        </div>

    </div >
)
}
