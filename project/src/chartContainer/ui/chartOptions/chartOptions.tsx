import React from 'react'

function ChartOptions({ changeOptions, data }) {
    return (
        <>
            <div className="flex w-full gap-5 ">
                <label htmlFor="">type:</label>
                <select className='w-full flex' onChange={(e) => changeOptions('type', e.target.value)}>
                    <option value="line">line</option>
                    <option value="spline">spline</option>
                    <option value="area">area</option>
                    <option value="areaspline">areaspline</option>
                    <option value="column">column</option>
                    <option value="bar">bar</option>
                    <option value="pie">pie</option>
                </select>
            </div>
            <div className="flex w-full gap-5 mt-5">
                <label htmlFor="">yAxisName:</label>
                <input className='w-full flex' defaultValue={data.yAxisName} onChange={(e) => changeOptions('yAxisName', e.target.value)} />
            </div>
            <div className="flex w-full gap-5 mt-5">
                <label htmlFor="">colorPallet:</label>
                <input className='w-full flex' type='color' defaultValue={data.colorPallete[0]} onChange={(e) => changeOptions('colorPallete', [e.target.value])} />
            </div>
            <div className="flex w-full gap-5 mt-5">
                <label htmlFor="">showLabel:</label>
                <input className='w-full flex' type='checkbox' defaultChecked={data.showLabel} onChange={(e) => changeOptions('showLabel', !data.showLabel)} />
            </div>
            <div className="flex w-full gap-5 mt-5">
                <label htmlFor="">showTooltip:</label>
                <input className='w-full flex' type='checkbox' defaultChecked={data.showTooltip} onChange={(e) => changeOptions('showTooltip', !data.showTooltip)} />
            </div>
        </>
    )
}

export default ChartOptions