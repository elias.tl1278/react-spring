import { HighchartsReact } from 'highcharts-react-official'
import Highcharts from 'highcharts'
import React from 'react'

function Chart({ data, options }) {
    return (
        <HighchartsReact highcharts={Highcharts} options={options(data.type)} />
    )
}

export default Chart