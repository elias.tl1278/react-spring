import { useRef, useState } from 'react'
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'
import './App.css'
import ChartMain from './chartContainer/feature/chartMain'

function App() {
  const [count, setCount] = useState(0)
 
  return (
    <>
     <ChartMain></ChartMain>
    </>
  )
}

export default App
