package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@SpringBootApplication
@RestController
public class DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    //    @GetMapping
//    public List<String> hello() {
//        return List.of("hello"," world");
//    }
    @CrossOrigin
    @GetMapping("/getData")
    public List<List<Number>> home() {
        return List.of(List.of(0,2,0),List.of(2,5,0),List.of(5,6,0));
    }
}
